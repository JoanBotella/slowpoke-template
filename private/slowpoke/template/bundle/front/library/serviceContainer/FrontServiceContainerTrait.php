<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\library\serviceContainer;

use slowpoke\template\bundle\front\page\about\library\serviceContainer\FrontAboutServiceContainerTrait;
use slowpoke\template\bundle\front\page\home\library\serviceContainer\FrontHomeServiceContainerTrait;
use slowpoke\template\bundle\front\page\notFound\library\serviceContainer\FrontNotFoundServiceContainerTrait;

use slowpoke\template\bundle\front\service\widget\layout\FrontLayoutWidgetServiceItf;
use slowpoke\template\bundle\front\service\widget\layout\FrontLayoutWidgetService;

trait FrontServiceContainerTrait
{
	use
		FrontAboutServiceContainerTrait,
		FrontHomeServiceContainerTrait,
		FrontNotFoundServiceContainerTrait
	;

	private FrontLayoutWidgetServiceItf $frontLayoutWidget;

	public function getFrontLayoutWidget():FrontLayoutWidgetServiceItf
	{
		if (!isset($this->frontLayoutWidget))
		{
			$this->frontLayoutWidget = $this->buildFrontLayoutWidget();
		}
		return $this->frontLayoutWidget;
	}

		protected function buildFrontLayoutWidget():FrontLayoutWidgetServiceItf
		{
			return new FrontLayoutWidgetService();
		}

}