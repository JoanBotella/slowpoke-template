<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\service\widget\layout;

use slowpoke\framework\library\widget\WidgetOutput;
use slowpoke\template\bundle\front\service\widget\layout\FrontLayoutWidgetInput;

interface FrontLayoutWidgetServiceItf
{

	public function run(FrontLayoutWidgetInput $input):WidgetOutput;

}