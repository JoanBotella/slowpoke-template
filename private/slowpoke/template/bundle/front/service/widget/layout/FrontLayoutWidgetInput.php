<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\service\widget\layout;

final class FrontLayoutWidgetInput
{

	private string $mainContent;

	public function hasMainContent():bool
	{
		return isset(
			$this->mainContent
		);
	}

	public function getMainContentAfterHas():string
	{
		return $this->mainContent;
	}

	public function setMainContent(string $v):void
	{
		$this->mainContent = $v;
	}

	public function unsetMainContent():void
	{
		unset(
			$this->mainContent
		);
	}

}