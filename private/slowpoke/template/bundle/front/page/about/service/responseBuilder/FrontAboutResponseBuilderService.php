<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\about\service\responseBuilder;

use slowpoke\template\bundle\front\library\responseBuilder\FrontResponseBuilderServiceAbs;
use slowpoke\template\bundle\front\page\about\service\responseBuilder\FrontAboutResponseBuilderServiceItf;
use slowpoke\template\bundle\front\page\about\service\responseBuilder\FrontAboutResponseBuilderInput;
use slowpoke\framework\library\responseBuilder\ResponseBuilderOutput;
use slowpoke\framework\service\widget\html\HtmlWidgetServiceItf;
use slowpoke\framework\service\widget\html\HtmlWidgetInput;
use slowpoke\template\bundle\front\service\widget\layout\FrontLayoutWidgetServiceItf;
use slowpoke\template\bundle\front\page\about\service\widget\mainContent\FrontAboutMainContentWidgetServiceItf;
use slowpoke\template\bundle\front\page\about\service\widget\mainContent\FrontAboutMainContentWidgetInput;
use slowpoke\core\library\Error;

final class FrontAboutResponseBuilderService extends FrontResponseBuilderServiceAbs implements FrontAboutResponseBuilderServiceItf
{
	const ERROR_CODE_NO_MAIN_CONTENT_WIDGET_TEXT = 0;

	private FrontAboutMainContentWidgetServiceItf $mainContentWidget;

	private FrontAboutResponseBuilderInput $input;

	public function __construct(
		HtmlWidgetServiceItf $htmlWidget,
		FrontLayoutWidgetServiceItf $layoutWidget,
		FrontAboutMainContentWidgetServiceItf $mainContentWidget
	)
	{
		parent::__construct(
			$htmlWidget,
			$layoutWidget
		);
		$this->mainContentWidget = $mainContentWidget;
	}

	public function run(FrontAboutResponseBuilderInput $input):ResponseBuilderOutput
	{
		$this->input = $input;

		$this->setupOutput();

		$output = $this->output;

		$this->tearDown();

		return $output;
	}

	protected function tryToSetupMainContentWidgetText():void
	{
		$mainContentWidgetOutput = $this->mainContentWidget->run(
			$this->buildMainContentWidgetInput()
		);

		if ($mainContentWidgetOutput->hasErrors())
		{
			$this->output->addErrors(
				$mainContentWidgetOutput->getErrorsAfterHas()
			);
			return;
		}

		if ($mainContentWidgetOutput->hasText())
		{
			$this->mainContentWidgetText = $mainContentWidgetOutput->getTextAfterHas();
			return;
		}

		$this->output->addError(
			new Error(
				self::ERROR_CODE_NO_MAIN_CONTENT_WIDGET_TEXT,
				'MainContentWidget had no errors but had no text'
			)
		);
	}

		private function buildMainContentWidgetInput():FrontAboutMainContentWidgetInput
		{
			$mainContentWidgetInput = new FrontAboutMainContentWidgetInput(
			);
			return $mainContentWidgetInput;
		}

	protected function buildHtmlWidgetInput():HtmlWidgetInput
	{
		// !!!

		$htmlWidgetInput = new HtmlWidgetInput(
			'en',
			'About'
		);

		$htmlWidgetInput->setPageCode('front-about');

		$htmlWidgetInput->setBodyContent(
			$this->layoutWidgetText
		);

		return $htmlWidgetInput;
	}

	protected function tearDown():void
	{
		unset($this->input);
		parent::tearDown();
	}

}