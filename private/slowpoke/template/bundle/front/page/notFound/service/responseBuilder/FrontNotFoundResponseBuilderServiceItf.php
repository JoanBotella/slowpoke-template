<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\notFound\service\responseBuilder;

use slowpoke\framework\library\responseBuilder\ResponseBuilderOutput;
use slowpoke\template\bundle\front\page\notFound\service\responseBuilder\FrontNotFoundResponseBuilderInput;

interface FrontNotFoundResponseBuilderServiceItf
{

	public function run(FrontNotFoundResponseBuilderInput $input):ResponseBuilderOutput;

}