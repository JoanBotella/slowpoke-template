<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\notFound\service\widget\mainContent;

use slowpoke\template\bundle\front\page\notFound\service\widget\mainContent\FrontNotFoundMainContentWidgetServiceItf;
use slowpoke\template\bundle\front\page\notFound\service\widget\mainContent\FrontNotFoundMainContentWidgetInput;
use slowpoke\framework\library\widget\WidgetOutput;

final class FrontNotFoundMainContentWidgetService implements FrontNotFoundMainContentWidgetServiceItf
{

	private FrontNotFoundMainContentWidgetInput $input;

	private WidgetOutput $output;

	private string $text;

	public function run(FrontNotFoundMainContentWidgetInput $input):WidgetOutput
	{
		$this->input = $input;

		$this->setupOutput();

		$output = $this->output;

		$this->tearDown();

		return $output;
	}

		private function setupOutput():void
		{
			$this->output = $this->buildOutput();

			$this->tryToSetupText();

			if ($this->output->hasErrors())
			{
				return;
			}

			$this->output->setText(
				$this->text
			);
		}

			private function buildOutput():WidgetOutput
			{
				return new WidgetOutput();
			}

			private function tryToSetupText():void
			{
				$this->text = $this->renderTemplate(
					$this->input
				);
			}

				private function renderTemplate(FrontNotFoundMainContentWidgetInput $input):string
				{
					ob_start();
					require $this->getTemplateFilePath();
					return ob_get_clean();
				}

					private function getTemplateFilePath():string
					{
						return __DIR__.'/template.php';
					}

		private function tearDown():void
		{
			unset($this->input);
			unset($this->output);
			unset($this->text);
		}

}