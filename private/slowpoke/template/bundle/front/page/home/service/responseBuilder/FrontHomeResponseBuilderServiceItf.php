<?php
declare(strict_types=1);

namespace slowpoke\template\bundle\front\page\home\service\responseBuilder;

use slowpoke\framework\library\responseBuilder\ResponseBuilderOutput;
use slowpoke\template\bundle\front\page\home\service\responseBuilder\FrontHomeResponseBuilderInput;

interface FrontHomeResponseBuilderServiceItf
{

	public function run(FrontHomeResponseBuilderInput $input):ResponseBuilderOutput;

}