<?php
declare(strict_types=1);

namespace slowpoke\template\library\route;

use slowpoke\framework\library\route\RouteItf;
use slowpoke\template\library\serviceContainer\ServiceContainerItf;

abstract class RouteAbs implements RouteItf
{

	protected ServiceContainerItf $serviceContainer;

	public function __construct(
		ServiceContainerItf $serviceContainer
	)
	{
		$this->serviceContainer = $serviceContainer;
	}

}
