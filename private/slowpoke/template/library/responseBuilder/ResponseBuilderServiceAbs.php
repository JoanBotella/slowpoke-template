<?php
declare(strict_types=1);

namespace slowpoke\template\library\responseBuilder;

use slowpoke\framework\service\widget\html\HtmlWidgetServiceItf;
use slowpoke\framework\library\responseBuilder\ResponseBuilderOutput;
use slowpoke\framework\library\response\Response;
use slowpoke\framework\service\widget\html\HtmlWidgetInput;
use slowpoke\core\library\HttpStatusConstant;

abstract class ResponseBuilderServiceAbs
{

	private HtmlWidgetServiceItf $htmlWidget;

	protected ResponseBuilderOutput $output;

	private Response $response;

	private string $htmlWidgetText;

	protected string $layoutWidgetText;

	protected string $mainContentWidgetText;

	public function __construct(
		HtmlWidgetServiceItf $htmlWidget
	)
	{
		$this->htmlWidget = $htmlWidget;
	}

	protected function setupOutput():void
	{
		$this->output = $this->buildOutput();

		$this->setupResponse();

		if ($this->output->hasErrors())
		{
			return;
		}

		$this->output->setResponse(
			$this->response
		);
	}

		private function buildOutput():ResponseBuilderOutput
		{
			return new ResponseBuilderOutput();
		}

		private function setupResponse():void
		{
			$this->tryToSetupMainContentWidgetText();

			if ($this->output->hasErrors())
			{
				return;
			}

			$this->tryToSetupLayoutWidgetText();

			if ($this->output->hasErrors())
			{
				return;
			}

			$this->tryToSetupHtmlWidgetText();

			if ($this->output->hasErrors())
			{
				return;
			}

			$this->response = new Response(
				$this->getHttpStatus(),
				$this->htmlWidgetText
			);
		}

			abstract protected function tryToSetupMainContentWidgetText():void;

			abstract protected function tryToSetupLayoutWidgetText():void;

			private function tryToSetupHtmlWidgetText():void
			{
				$htmlWidgetOutput = $this->htmlWidget->run(
					$this->buildHtmlWidgetInput()
				);

				if ($htmlWidgetOutput->hasErrors())
				{
					$this->output->addErrors(
						$htmlWidgetOutput->getErrorsAfterHas()
					);
					return;
				}

				if ($htmlWidgetOutput->hasText())
				{
					$this->htmlWidgetText = $htmlWidgetOutput->getTextAfterHas();
					return;
				}

				$this->output->addError(
					new Error('htmlWidget had no errors but had no text')
				);
			}

				abstract protected function buildHtmlWidgetInput():HtmlWidgetInput;

			protected function getHttpStatus():int
			{
				return HttpStatusConstant::OK;
			}

	protected function tearDown():void
	{
		unset($this->output);
		unset($this->response);
		unset($this->htmlWidgetText);
		unset($this->layoutWidgetText);
		unset($this->mainContentWidgetText);
	}

}