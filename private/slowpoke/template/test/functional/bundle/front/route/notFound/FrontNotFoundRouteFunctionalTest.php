<?php
declare(strict_types=1);

namespace slowpoke\template\test\functional\bundle\front\route\notFound;

use PHPUnit\Framework\TestCase;
use slowpoke\framework\library\request\Request;
use slowpoke\framework\service\app\AppInput;
use slowpoke\framework\service\app\AppOutput;
use slowpoke\core\library\HttpStatusConstant;
use slowpoke\template\library\serviceContainer\ServiceContainer;
use slowpoke\template\library\serviceContainer\ServiceContainerItf;
use slowpoke\template\test\functional\bundle\front\route\notFound\FrontNotFoundResponseBodyChecker;

final class FrontNotFoundRouteFunctionalTest extends TestCase
{

	private ServiceContainerItf $serviceContainer;

	public function __construct(
		$name = null,
		$data = [],
		$dataName = ''
	)
	{
		parent::__construct(
			$name,
			$data,
			$dataName
		);
		$this->serviceContainer = new ServiceContainer();
	}

	public function test_requestingFrontNotFound_returnsAnOutputWithoutErrors():void
	{
		$output = $this->runAppWithRequestToFrontNotFound();
		$this->assertFalse(
			$output->hasErrors()
		);
	}

		private function runAppWithRequestToFrontNotFound():AppOutput
		{
			$app = $this->serviceContainer->getApp();
			return $app->run(
				$this->buildAppInputWithRequestToFrontNotFound()
			);
		}

			private function buildAppInputWithRequestToFrontNotFound():AppInput
			{
				$input = new AppInput(
					$this->buildRequestToFrontNotFound()
				);
				return $input;
			}

				private function buildRequestToFrontNotFound():Request
				{
					return new Request('non-existent-route');
				}

	public function test_requestingFrontNotFound_outputsAResponse():void
	{
		$output = $this->runAppWithRequestToFrontNotFound();
		$this->assertTrue(
			$output->hasResponse()
		);
	}

	public function test_requestingFrontNotFound_outputsAResponseWithNotFoundHttpStatus():void
	{
		$output = $this->runAppWithRequestToFrontNotFound();
		$response = $output->getResponseAfterHas();
		$this->assertEquals(
			HttpStatusConstant::NOT_FOUND,
			$response->getHttpStatus()
		);
	}

	public function test_requestingFrontNotFound_outputsTheExpectedResponseBody():void
	{
		$output = $this->runAppWithRequestToFrontNotFound();
		$response = $output->getResponseAfterHas();
		$responseBodyChecker = new FrontNotFoundResponseBodyChecker();
		$this->assertTrue(
			$responseBodyChecker->check(
				$response->getBody()
			)
		);
	}

}
