<?php
declare(strict_types=1);

namespace slowpoke\template\test\functional\bundle\front\route\about;

use PHPUnit\Framework\TestCase;
use slowpoke\framework\library\request\Request;
use slowpoke\framework\service\app\AppInput;
use slowpoke\framework\service\app\AppOutput;
use slowpoke\core\library\HttpStatusConstant;
use slowpoke\template\library\serviceContainer\ServiceContainer;
use slowpoke\template\library\serviceContainer\ServiceContainerItf;
use slowpoke\template\test\functional\bundle\front\route\about\FrontAboutResponseBodyChecker;

final class FrontAboutRouteFunctionalTest extends TestCase
{

	private ServiceContainerItf $serviceContainer;

	public function __construct(
		$name = null,
		$data = [],
		$dataName = ''
	)
	{
		parent::__construct(
			$name,
			$data,
			$dataName
		);
		$this->serviceContainer = new ServiceContainer();
	}

	public function test_requestingFrontAbout_returnsAnOutputWithoutErrors():void
	{
		$output = $this->runAppWithRequestToFrontAbout();
		$this->assertFalse(
			$output->hasErrors()
		);
	}

		private function runAppWithRequestToFrontAbout():AppOutput
		{
			$app = $this->serviceContainer->getApp();
			return $app->run(
				$this->buildAppInputWithRequestToFrontAbout()
			);
		}

			private function buildAppInputWithRequestToFrontAbout():AppInput
			{
				$input = new AppInput(
					$this->buildRequestToFrontAbout()
				);
				return $input;
			}

				private function buildRequestToFrontAbout():Request
				{
					return new Request('about');
				}

	public function test_requestingFrontAbout_outputsAResponse():void
	{
		$output = $this->runAppWithRequestToFrontAbout();
		$this->assertTrue(
			$output->hasResponse()
		);
	}

	public function test_requestingFrontAbout_outputsAResponseWithOkHttpStatus():void
	{
		$output = $this->runAppWithRequestToFrontAbout();
		$response = $output->getResponseAfterHas();
		$this->assertEquals(
			HttpStatusConstant::OK,
			$response->getHttpStatus()
		);
	}

	public function test_requestingFrontAbout_outputsTheExpectedResponseBody():void
	{
		$output = $this->runAppWithRequestToFrontAbout();
		$response = $output->getResponseAfterHas();
		$responseBodyChecker = new FrontAboutResponseBodyChecker();
		$this->assertTrue(
			$responseBodyChecker->check(
				$response->getBody()
			)
		);
	}

}
