<?php
declare(strict_types=1);

namespace slowpoke\core\service\stringReplacer;

use slowpoke\core\service\stringReplacer\StringReplacerServiceItf;
use slowpoke\core\service\stringReplacer\StringReplacerInput;
use slowpoke\core\service\stringReplacer\StringReplacerOutput;

final class StringReplacerService implements StringReplacerServiceItf
{

	private StringReplacerInput $input;

	private StringReplacerOutput $output;

	private string $text;

	public function run(StringReplacerInput $input):StringReplacerOutput
	{
		$this->input = $input;

		$this->setupOutput();

		$output = $this->output;

		$this->tearDown();

		return $output;
	}

		private function setupOutput():void
		{
			$this->output = $this->buildOutput();

			$this->tryToSetupText();

			if ($this->output->hasErrors())
			{
				return;
			}

			$this->output->setText(
				$this->text
			);
		}

			private function buildOutput():StringReplacerOutput
			{
				return new StringReplacerOutput();
			}

			private function tryToSetupText():void
			{
				$this->text = str_replace(
					$this->input->getSearch(),
					$this->input->getReplacement(),
					$this->input->getText()
				);
			}

		private function tearDown():void
		{
			unset($this->input);
			unset($this->output);
			unset($this->text);
		}

}