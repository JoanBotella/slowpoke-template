<?php
declare(strict_types=1);

namespace slowpoke\core\service\stringReplacer;

use slowpoke\core\library\serviceOutput\ServiceOutputAbs;

final class StringReplacerOutput extends ServiceOutputAbs
{

	private string $text;

	public function hasText():bool
	{
		return isset(
			$this->text
		);
	}

	public function getTextAfterHas():string
	{
		return $this->text;
	}

	public function setText(string $v):void
	{
		$this->text = $v;
	}

	public function unsetText():void
	{
		unset(
			$this->text
		);
	}

}