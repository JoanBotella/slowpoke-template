<?php
declare(strict_types=1);

namespace slowpoke\core\library\serviceContainer;

use slowpoke\core\service\stringReplacer\StringReplacerServiceItf;

interface CoreServiceContainerItf
{

	public function getStringReplacer():StringReplacerServiceItf;

}
