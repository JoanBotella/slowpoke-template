<?php
declare(strict_types=1);

namespace slowpoke\core\library;

final class Error
{

	private int $code;

	private string $message;

	private array $backtrace;

	public function __construct(
		int $code,
		string $message
	)
	{
		$this->code = $code;
		$this->message = $message;
		$this->backtrace = debug_backtrace();
	}

	public function getCode():int
	{
		return $this->code;
	}

	public function getMessage():string
	{
		return $this->message;
	}

	public function getBacktrace():array
	{
		return $this->backtrace;
	}

	public function getClass():string
	{
		return $this->backtrace[1]['class'];
	}

}