<?php
declare(strict_types=1);

namespace slowpoke\core\library\serviceOutput;

use slowpoke\core\library\Error;

interface ServiceOutputItf
{

	public function addError(Error $error):void;

	public function addErrors(array $errors):void;

	public function hasErrors():bool;

	public function getErrorsAfterHas():array;

	public function unsetErrors():void;

}