<?php
declare(strict_types=1);

namespace slowpoke\core\library\serviceOutput;

use slowpoke\core\library\Error;
use slowpoke\core\library\serviceOutput\ServiceOutputItf;

abstract class ServiceOutputAbs implements ServiceOutputItf
{

	private array $errors;

	public function addError(Error $error):void
	{
		if (!isset($this->errors))
		{
			$this->errors = [];
		}
		$this->errors[] = $error;
	}

	public function addErrors(array $errors):void
	{
		foreach ($errors as $error)
		{
			$this->addError($error);
		}
	}

	public function hasErrors():bool
	{
		return isset(
			$this->errors
		);
	}

	public function getErrorsAfterHas():array
	{
		return $this->errors;
	}

	public function unsetErrors():void
	{
		unset(
			$this->errors
		);
	}

}