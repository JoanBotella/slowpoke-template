<?php
declare(strict_types=1);

namespace slowpoke\core\library;

class StringToStringMapWrapper
{

	private array $map;

	public function __construct(
		array $map
	)
	{
		$this->map = $map;
	}

	public function hasKey(string $key):bool
	{
		return isset(
			$this->map[$key]
		);
	}

	public function getKeyAfterHas(string $key):string
	{
		return $this->map[$key];
	}

	public function setKey(string $key, string $value):void
	{
		$this->map[$key] = $value;
	}

	public function unsetKey(string $key):void
	{
		unset(
			$this->map[$key]
		);
	}

}