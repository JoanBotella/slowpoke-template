<?php
declare(strict_types=1);

use slowpoke\framework\service\widget\html\HtmlWidgetInput;

/**
 * @var HtmlWindgetInput $input
 */

?><!DOCTYPE html>
<html lang="<?= $input->getLangCode() ?>"<?php if ($input->hasPageCode()): ?> data-page="<?= $input->getPageCodeAfterHas() ?>" class="<?= $input->getPageCodeAfterHas() ?>"<?php endif; ?>>
<head>
	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<?php if ($input->hasFaviconUrl()): ?>
	<link rel="icon" href="<?= $input->getFaviconUrlAfterHas() ?>" type="image/png" />
	<?php endif; ?>

	<title><?= $input->getHeadTitle() ?></title>

	<?php
		if ($input->hasStyleUrls()):
			foreach ($input->getStyleUrlsAfterHas() as $url):
	?>
	<link href="<?= $url ?>" rel="stylesheet" media="screen" />
	<?php
			endforeach;
		endif;
	?>

</head>
<body>

	<?php if ($input->hasBodyContent()): ?>
	<?= $input->getBodyContentAfterHas() ?>
	<?php endif; ?>

	<?php
		if ($input->hasScriptUrls()):
			foreach ($input->getScriptUrlsAfterHas() as $url):
	?>
	<script src="<?= $url ?>"></script>
	<?php
			endforeach;
		endif;
	?>

</body>
</html>