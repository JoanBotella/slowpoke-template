<?php
declare(strict_types=1);

namespace slowpoke\framework\service\responseDispatcher;

use slowpoke\core\library\serviceOutput\ServiceOutputAbs;

final class ResponseDispatcherOutput extends ServiceOutputAbs
{
}