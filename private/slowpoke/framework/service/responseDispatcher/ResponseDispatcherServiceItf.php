<?php
declare(strict_types=1);

namespace slowpoke\framework\service\responseDispatcher;

use slowpoke\framework\service\responseDispatcher\ResponseDispatcherOutput;
use slowpoke\framework\service\responseDispatcher\ResponseDispatcherInput;

interface ResponseDispatcherServiceItf
{

	public function run(ResponseDispatcherInput $input):ResponseDispatcherOutput;

}