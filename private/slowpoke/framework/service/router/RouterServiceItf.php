<?php
declare(strict_types=1);

namespace slowpoke\framework\service\router;

use slowpoke\framework\service\router\RouterOutput;
use slowpoke\framework\service\router\RouterInput;

interface RouterServiceItf
{

	public function route(RouterInput $input):RouterOutput;

}