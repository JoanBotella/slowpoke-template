<?php
declare(strict_types=1);

namespace slowpoke\framework\service\router;

use slowpoke\framework\library\request\Request;

final class RouterInput
{

	private Request $request;

	public function __construct(
		Request $request
	)
	{
		$this->request = $request;
	}

	public function getRequest():Request
	{
		return $this->request;
	}

}