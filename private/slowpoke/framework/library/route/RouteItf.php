<?php
declare(strict_types=1);

namespace slowpoke\framework\library\route;

use slowpoke\framework\library\controller\ControllerServiceItf;
use slowpoke\framework\library\requestMatcher\RequestMatcherServiceItf;

interface RouteItf
{

	public function getRequestMatcher():RequestMatcherServiceItf;

	public function getController():ControllerServiceItf;

}
