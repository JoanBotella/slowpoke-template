<?php
declare(strict_types=1);

namespace slowpoke\framework\library\requestMatcher;

use slowpoke\framework\library\requestMatcher\RequestMatcherServiceItf;
use slowpoke\framework\library\requestMatcher\RequestMatcherInput;
use slowpoke\framework\library\requestMatcher\RequestMatcherOutput;
use slowpoke\framework\library\request\Request;

abstract class RequestMatcherServiceAbs implements RequestMatcherServiceItf
{

	private RequestMatcherInput $input;

	private RequestMatcherOutput $output;

	protected bool $isMatch;

	public function match(RequestMatcherInput $input):RequestMatcherOutput
	{
		$this->input = $input;

		$this->setupOutput();

		$output = $this->output;

		$this->tearDown();

		return $output;
	}

		private function setupOutput():void
		{
			$this->output = $this->buildOutput();

			$this->setupIsMatch();

			if (isset($this->isMatch))
			{
				$this->output->setIsMatch(
					$this->isMatch
				);
			}
		}

			private function buildOutput():RequestMatcherOutput
			{
				return new RequestMatcherOutput();
			}

			protected function setupIsMatch():void
			{
			}

		private function tearDown():void
		{
			unset($this->input);
			unset($this->output);
			unset($this->isMatch);
		}

	protected function getRequest():Request
	{
		return $this->input->getRequest();
	}

	protected function getRequestPath():string
	{
		return $this->getRequest()->getPath();
	}

	protected function isRequestPathEqualTo(string $path):bool
	{
		return $this->getRequest()->getPath() == $path;
	}

}
