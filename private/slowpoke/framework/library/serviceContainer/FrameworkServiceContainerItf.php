<?php
declare(strict_types=1);

namespace slowpoke\framework\library\serviceContainer;

use slowpoke\framework\service\app\AppServiceItf;
use slowpoke\framework\service\router\RouterServiceItf;
use slowpoke\framework\service\requestBuilder\RequestBuilderServiceItf;
use slowpoke\framework\service\responseDispatcher\ResponseDispatcherServiceItf;
use slowpoke\framework\service\cgiAppRunner\CgiAppRunnerServiceItf;
use slowpoke\framework\service\widget\html\HtmlWidgetServiceItf;

interface FrameworkServiceContainerItf
{

	public function getApp():AppServiceItf;

	public function getRouter():RouterServiceItf;

	public function getRequestBuilder():RequestBuilderServiceItf;

	public function getResponseDispatcher():ResponseDispatcherServiceItf;

	public function getCgiAppRunner():CgiAppRunnerServiceItf;

	public function getHtmlWidget():HtmlWidgetServiceItf;

}
