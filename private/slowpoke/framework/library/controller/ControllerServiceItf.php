<?php
declare(strict_types=1);

namespace slowpoke\framework\library\controller;

use slowpoke\framework\library\controller\ControllerInput;
use slowpoke\framework\library\controller\ControllerOutput;

interface ControllerServiceItf
{

	public function action(ControllerInput $input):ControllerOutput;

}